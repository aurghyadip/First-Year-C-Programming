# C Programming (1st or 2nd Semester)
Here's a repository of all the sample C programs that is very likely to be done in your college if you are a MAKUT Student. And if you are not, then these programs will help you learn the very basic concepts of C programming. C is a beautiful language and is very strictly typed language.



Before moving deep in the repository, you should know how a C program works and how you can compile it. 

## Linux
If you are using any **Linux** or **Unix** based operating system, then do the following.

Open up your terminal, type `nano helloworld.c` or `vi helloworld.c` and press <kbd>Enter</kbd> 

A very basic command line text editor will open up for you. Type the following code in it.
```C
#include<stdio.h>
int main()
{
	printf("Hello World!");
}
```
Now, after typing in the code, save the file and exit from the editor. If you are using `nano` then it's <kbd>Ctrl</kbd> + <kbd>x</kbd>.
And in `vim` it's <kbd>Esc</kbd> , then `:wq` and press <kbd>Enter</kbd>.

After you have exited to the terminal. Type in `cc helloworld.c` and press <kbd>Enter</kbd>. (This is called compiling process, if you have typed in the hello world program properly, it will just create a new line, otherwise it'll show errors).
**`cc` is actually GNU GCC Compiler**.

After that, press `./a.out` in the terminal and press <kbd>Enter</kbd> and it'll show
```
Hello World!
```
**Congratulations! You've done your first C Program.**

## Windows
Unfortunately for you , Windows doesn't support compilation of C codes natively. You have to download a compiler and/or and IDE for compiling and typing the code.
I personally prefer Code::Blocks as it's very easy to learn and to work with. You can download the latest build of Code::Blocks with the **Ming GCC for Windows** compiler from this link - [http://www.codeblocks.org](http://www.codeblocks.org). Download the `.exe` file which looks like this `codeblocks-XX.XXmingw-setup.exe` where `XX` denotes the version number.

---

After downloading and setting up Code::Blocks, you can easily create, compile and run a C source code.

Now that you're all set up, you can move into the assignments folders for various examples of C programs. 
Happy Coding :smile:
